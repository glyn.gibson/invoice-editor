2017-06-12

## Problem Description

Build an invoice editor that allows a user add, edit, or remove line items
according to the text-based mockup below. Pricing should be updated on-the-fly
as line items are added or edited. Don't worry about persisting invoices.

```

Item                      Qty     Price       Total
--------------------------------------------------------
Widget                   [ 2 ]  [ $10.00 ]  [ $20.00 ] x
Cog                      [ 2 ]  [ $15.99 ]  [ $31.98 ] x
[ New Item ]             [   ]  [        ]  [        ]

                                    --------------------
                                    Subtotal      $51.98
                                    Tax (5%)       $2.60
                                    Total         $54.58
                                    --------------------
```

## Instructions

Requires `nodejs`  and `npm` (tested with 6.10.3 and 3.10.10, respectively)

1. Install dependencies with `npm` 

```
$ npm install
``` 

2. To run the debug server:

```
$ npm start
```

3. To build a distribution

```
$ npm run build
```

4. To run tests

  * Start test runner
    ```
    $ npm test
    ```

  * Press `a` to run all test suites
  
  * NB: test runner may run into inotify/filesystem watch limits on Ubuntu;  this should be fixable by running `npm dedup` and/or updating the number of watches as per [these instructions](https://webpack.github.io/docs/troubleshooting.html#not-enough-watchers)


## Technology & Design Notes 

This project was bootstrapped
with [Create React App](https://github.com/facebookincubator/create-react-app),
and uses Redux for state management, Webpack + Babel under the hood for ES6
transpiling, dist building, etc., Jest for testing, and the Materialize CSS
framework for lightweight Material-style styling.

File and component layout was set up based on 'Rails-style' examples in the
Redux docs, with separate directories for
reducers/components/containers/actions. The contents of the `items` reducer were
originally split up into `items` and `summary` reducers, but the latter reducer
depended on state from the former, and only handled one action, so they were
joined back into a fat reducer.


## Limitations & Outstanding TODOs

Materialize currently requires Google's Material icons, which are being
imported from their CDN in the `index.html` template in `public/`. These
should be integrated so that they're available while offline.

Extending validation is somewhat limited by the present implementation, and by
inconsistent behaviour in Materialize's HTML5 validation handling. The inline
validation and `price` / `priceDisplay` fields in the `Item` components should
be replaced by a proper component abstraction to manage presentation vs. model
state and event handling for each `<input>` in `Item`. This could be implemented
using [redux-form](http://redux-form.com/6.8.0/), or through a custom component
system, and would allow for further customization/flexible.

Validation is currently allowing non-whole numbers for `quantity` field (e.g.
for hours), but flags negatives and restricts inputs for non-numbers. At
present, input for the `price` field is restricted by the reducer. We can also
probably better handle summary display for large totals, and/or set limits on
such values.

Present tests primarily cover domain logic in the reducer, and should be
extended to test components, containers, etc. Also, reducers can probably be
further refactored.

