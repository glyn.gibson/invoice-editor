import {
    blankItem,
    items,
    initState,
    updateItemTotal,
    formatPriceDisplay,
    updateSummaryTotals,
    updatePrice,
    toCents
} from './items';

import {
    itemSetName,
    itemAdd,
    itemSetQty,
    itemSetPrice,
    itemDelete,
    itemUpdatePriceDisplay,
} from '../actions';

import _ from 'lodash';


function pipeState(fns, state = initState) {
    return _.flow(fns)(state);
}

describe('Items reducer', () => {
    describe('General behaviour', () => {
        it('should handle initial state', () => {
            expect(items(undefined, {}))
                .toEqual(initState);
        });
        it('should handle the full scenario outlined in the mockup', () => {
            const state = pipeState([
                x => items(x, itemSetName(0, 'Widget')),
                x => items(x, itemSetQty(0, 2)),
                x => items(x, itemSetPrice(0, 10)),
                x => items(x, itemAdd('Cog')),
                x => items(x, itemSetName(1, 'Cog')),
                x => items(x, itemSetQty(1, 2)),
                x => items(x, itemSetPrice(1, 15.99)),
            ]);
            expect(state.summary.total).toEqual(5458);
        });
    });
    describe('ITEM_SET_NAME handling', () => {
        it('should update item name after receiving ITEM_SET_NAME ', () => {
            const updatedState = items(initState, itemSetName(0, 'test'));
            expect(updatedState.items.find(x => x.id === 0 ).name)
                .toEqual('test');
        });
        it('should should set dirty flag after receiving ITEM_SET_NAME ', () => {
            const updatedState = items(initState, itemSetName(0, 'test'));
            expect(updatedState.items.find(x => x.id === 0 ).dirty)
                .toEqual(true);
        });
    });
    describe('ITEM_ADD handling', () => {
        it('should add a blank item on receiving ITEM_ADD', () => {
            const updatedState = items(initState, itemAdd());
            expect(updatedState.items.length).toEqual(2);
        });
        it('should increment ID counter on each add', () => {
            const firstUpdate = items(initState, itemAdd());
            const secondUpdate = items(firstUpdate, itemAdd());

            // TODO(ggibson): NB: right now, counter keeps increasing on each
            // invocation throughout the test suite.  This should get resolved
            expect(secondUpdate.items.length).toEqual(3);
            expect(secondUpdate.items[2].id)
                .toEqual(secondUpdate.items[1].id + 1);
        });
    });
    describe('ITEM_DELETE handling', () => {
        const newItem = {...blankItem(), name: 'newItem', id: 1};
        const newItems = [...initState.items, newItem ];
        const stateWithAdd = {...initState, items: newItems };

        it('should remove an item on ITEM_ADD delete', () => {
            expect(stateWithAdd.items.length).toEqual(2);
            const stateWithDelete = items(stateWithAdd, itemDelete(1));
            expect(stateWithDelete.items.length).toEqual(1);
        });
        it('handle case where ID does not exist', () => {
            const stateWithDelete = items(initState, itemDelete(1));
            expect(stateWithDelete).toEqual(initState);
        });
        it('removing last item replaces it w/ blankItem', () => {
            const stateWithUpdateName = items(stateWithAdd, itemSetName(0, 'test'));
            const stateWithDeletions = items(items(stateWithUpdateName, itemDelete(1)),
                                            itemDelete(0));

            expect(stateWithDeletions.items.length).toEqual(1);
            expect(stateWithDeletions.items[0]).toEqual(blankItem());
        });

    });
    describe('ITEM_SET_QTY handling', () => {
        it('should update qty field for updated item', () => {
            const updatedState = items(initState, itemSetQty(0, 1));
            expect(updatedState.items[0].qty).toEqual(1);
        });
        it('should update total for item (in cents)', () => {
            const updatedState = items(items(initState, itemSetQty(0, 1)),
                                       itemSetPrice(0, 2));
            expect(updatedState.items[0].total).toEqual(200);
        });
        it('passing in bad ID to action gets ignored', () => {
            const updatedState = items(initState, itemSetQty(99, 1));
            expect(updatedState.items[0].qty).toEqual(0);
        });
    });
    describe('ITEM_SET_PRICE handling', () => {
        it('should update price field for updated item (in cents)', () => {
            const updatedState = items(initState, itemSetPrice(0, 1));
            expect(updatedState.items[0].price).toEqual(100);
        });
        it('passing in bad ID to action gets ignored', () => {
            const updatedState = items(initState, itemSetQty(99, 1));
            expect(updatedState.items[0].price).toEqual(0);
        });
        it('display price should not change', () => {
            const updatedState = items(initState, itemSetPrice(0, 1));
            expect(updatedState.items[0].priceDisplay).toEqual(1);
        });
    });
    describe('ITEM_UPDATE_PRICE_DISPLAY handling', () => {
        it('should update the display value for price', () => {
            const updatedState = pipeState([
                x => items(x, itemSetPrice(0, 1)),
                x => items(x, itemUpdatePriceDisplay(0, 'does not matter'))
            ]);
            expect(updatedState.items[0].priceDisplay).toEqual('$ 1.00');
        });
    });

    describe('updateSummaryTotals()', () => {
        it('should update summary by summing item totals and applying tax', () => {
            const state = {
                ...initState,
                items: [
                    { name: 'item1', total: 300},
                    { name: 'item2', total: 400},
                ]
            };
            expect(updateSummaryTotals(state).summary)
                .toEqual({
                    subtotal: 700,
                    tax: 35.0,
                    taxRate: 0.05,
                    total: 735
                });
        });
        it('test idempotency', () => {
            expect(updateSummaryTotals(initState).summary)
                .toEqual(initState.summary);
        });
        it('should clamp tax at zero if subtotal goes negative', () => {
            const state = {
                ...initState,
                items: [
                    { name: 'a small item', total: 10},
                    { name: 'a big refund', total: -500}
                ]
            };
            expect(updateSummaryTotals(state).summary)
                .toEqual({
                    subtotal: -490,
                    tax: 0,
                    taxRate: 0.05,
                    total: -490
                });
        });

    });
    describe('formatPriceDisplay()', () => {
        it('should convert a price value in cents into a formatted string', () => {
            const items = [{...blankItem(), qty: 1, price: 200, id: 0}];
            const result = formatPriceDisplay(items, { id: 0});
            expect(result[0].priceDisplay).toEqual('$ 2.00');
        });
        it('behaviour is current undefined for a bad ID', () => {
            const items = [{...blankItem(), qty: 1, price: 200, id: 0}];
            expect(() => {
                const result = formatPriceDisplay(items, { id: 999});
            }).toThrow();
        });
    });
    describe('updatePrice()', () => {
        it('internal price should be converted to cents, while display price stays same', () => {
            const result = updatePrice(initState.items, { id: 0, price: 50});
            expect(result[0].price).toEqual(5000);
            expect(result[0].priceDisplay).toEqual(50);
        });
        it('only update price if it has changed', () => {
            const result = updatePrice(initState.items, { id: 0, price: 50});
            expect(result[0].price).toEqual(5000);
            expect(result[0].priceDisplay).toEqual(50);
            const updated = updatePrice(result, { id: 0, price: 45.5});
            expect(updated[0].price).toEqual(4550);
            expect(updated[0].priceDisplay).toEqual(45.5);
            const updatedAgain = updatePrice(result, { id: 0, price: 45.5});
            expect(updatedAgain[0].price).toEqual(4550);
            expect(updatedAgain[0].priceDisplay).toEqual(45.5);
        });

        it('stores invalid prices as 0, but keeps display prices visible for editing', () => {
            const result = pipeState([
                x => updatePrice(initState.items, { id: 0, price: 50}),
                x => updatePrice(initState.items, { id: 0, price: 'garbage'})
            ]);
            expect(result[0].price).toEqual(0);
            expect(result[0].priceDisplay).toEqual('garbage');
        });
        it('clamps negative prices to zero', () => {
            const result = updatePrice(initState.items, { id: 0, price: -1});
            expect(result[0].price).toEqual(0);
        });
    });
    describe('updateItemTotal()', () => {
        it('should update item total', () => {
            const items = [{...blankItem(), qty: 1, price: 2, id: 0 }];
            const result = updateItemTotal(items, { id: 0 });
            expect(result[0].total).toEqual(2);
        });
    });
    describe('toCents()', () => {
        it('should convert unparseable input to zero', () => {
            expect(toCents('garbage')).toEqual(0);
            expect(toCents('$ garbage')).toEqual(0);
        });
        it('should handle dollar sign-prefixed strings', () => {
            expect(toCents('$ 1')).toEqual(100);
            expect(toCents('$ 1.0')).toEqual(100);
            expect(toCents('$ 1.00')).toEqual(100);
            expect(toCents('$ 0.01')).toEqual(1);
        });
        it('should convert numbers', () => {
            expect(toCents(1)).toEqual(100);
            expect(toCents(0.01)).toEqual(1);
        });
        it('should round to nearest cent ', () => {
            expect(toCents(0.001)).toEqual(0);
            expect(toCents(0.004)).toEqual(0);
            expect(toCents(0.005)).toEqual(1);
        });
        it('coerces strings with number prefixes to numbers', () => {
            // unexpected side effect, but automatic translation
            // of numeric prefix to valid values rather than 0
            // prevents having to re-enter the value
            expect(toCents('$ 1.x')).toEqual(100);
            expect(toCents('$ 1.2x')).toEqual(120);
            expect(toCents('$ 0.01asdfasdfasdf')).toEqual(1);
        });
    });
});
