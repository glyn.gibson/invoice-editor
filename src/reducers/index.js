// @flow
import { items, initState } from './items';

/** root reducer -- just delegates to items() */
const invoiceApp = (state = initState, action) => ({
    ...state,
    ...items(state, action)
});
export default invoiceApp;
