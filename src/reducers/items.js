import {
    ITEM_SET_QTY,
    ITEM_SET_PRICE,
    ITEM_SET_NAME,
    ITEM_DELETE,
    ITEM_UPDATE_PRICE_DISPLAY,
    ITEM_ADD,
} from '../actions/';

////////////////////////////////////////////////////////////////////////////////
// state initialization

export const blankItem = () => ({
    id: 0,
    name: '',
    qty: 0,
    price: 0,
    priceDisplay: '0',
    total: 0,
    dirty: false
});

export const initSummary = () => ({
    subtotal: 0,
    tax: 0,
    total: 0,
    taxRate: 0.05
});

export const initState = { items: [ blankItem() ], summary: initSummary() };

////////////////////////////////////////////////////////////////////////////////
// helpers

/**
 * Map function to a state if id matches
 * @param items - List of items to map over
 * @param id - ID to match
 * @param fn - function to apply
 * @returns Copy of array
 */
function withItem(items, id, fn)  {
    return items.map(item => {
        if (item.id === id) {
            return {...item, ...fn(item)};
        } else {
            return item;
        }
    });
}

/**
 * Converts price input to value in cents, attempting to interpret string values
 *
 * @param price - Price (as number or as string value)
 * @returns price in cents, or 0 if the input can't be converted
 */
export function toCents(price) {
    const stripped = typeof(price) === 'string' ? price.replace('$', '') : price;
    const asFloat = isNaN(parseFloat(stripped)) ? 0 : parseFloat(stripped);
    const asCents = Math.round(asFloat * 100);
    return asCents;
}

/**
 * Update an array by ID
 * @param items - array to update
 * @param id - ID of item to update
 * @param update - object w/ properties to update
 * @returns Copy of array w/ updated item
 */
function updateById (items, id, update) {
    return items.map(item => {
        if (item.id === id) {
            return {...item, ...update};
        } else {
            return item;
        }
    });
};

/**
 * Create a reducer function based on callback lookup table
 *
 * As per Redux 'reducing boilerplate' example
 *
 * @param initialState - initial reducer state
 * @param handlers - array of action type -> reducer callback objects
 * @returns Reducer function
 */
function createReducer(initialState, handlers) {
    return function reducer(state = initialState, action) {
        if (handlers.hasOwnProperty(action.type)) {
            return handlers[action.type](state, action);
        } else {
            return state;
        }
    };
}


////////////////////////////////////////////////////////////////////////////////
// shared helpers

/**
 * Update item price
 *
 * Calculates internal price + display value
 * @param items - list of items
 * @param action - update action w/ `id` and `price` properties
 * @return updated state
 */
export function updatePrice(items, action) {
    const priceDisplay = action.price;

    // update the visual display of the form
    const priceDisplayUpdated = updateById(items, action.id, { priceDisplay: priceDisplay});
    const clamped = priceDisplay < 0 ? 0 : priceDisplay;
    const asCents = toCents(clamped) < 0 ? 0 : toCents(clamped);
    return updateById(priceDisplayUpdated, action.id, { price: asCents});
}

/**
 * Formats displayPrice with leading '$' and fixed 2 digits
 *
 * @param items - list of items
 * @param action - action with `id` property
 * @returns new state object with updated item
 */
export function formatPriceDisplay(items, action) {
    const currentItem = items.find(x => x.id === action.id);
    const priceFromCents = currentItem.price / 100;
    const newPriceDisplay = `$ ${priceFromCents.toFixed(2)}`;
    return updateById(items, action.id, { priceDisplay: newPriceDisplay});
}


/**
 * Updates summary portion of state, summing items and calculating tax
 *
 * @param state - entire state object
 * @returns new state object with updated `summary` state slice
 */
export function updateSummaryTotals(state) {
    const subtotal = state.items.reduce((acc, curr) => acc + curr.total, 0);
    const tax = Math.max(Math.round(state.summary.taxRate * subtotal),
                         0);
    const total = subtotal + tax;
    const newSummary = { subtotal, tax, total, taxRate: state.summary.taxRate };
    return {...state, summary: newSummary };
}


/**
 * Updates total for an item, by ID
 * @param items - array of items
 * @param action - action with `id` property
 * @returns updated copy of items array
 */
export function updateItemTotal(items, action) {
    return withItem(items, action.id, x => ({ total: x.qty * x.price }));
}

////////////////////////////////////////////////////////////////////////////////
// case reducers

/** Reducer for setting item name */
function itemSetName(state, action) {
    const itemsWithUpdatedName = updateById(state.items,
                                            action.id,
                                            {name: action.name, dirty: true});
    return {...state, items: itemsWithUpdatedName };
}

/** Reducer for adding new item */
function itemAdd(state, action) {
    const newItem = {...blankItem(), id: action.id };
    const itemsWithNewItem = [...state.items, newItem];
    return {...state, items: itemsWithNewItem};
}

/** Reducer for deleting item */
function itemDelete(state, action) {
    const filteredItems = state.items.filter(x => x.id !== action.id);
    const withNewItem = filteredItems.length === 0 ? [ blankItem() ] : filteredItems;
    return updateSummaryTotals({...state, items: withNewItem});
}

/** Reducer for setting item quantity */
function itemSetQty(state, action) {
    const itemsWithUpdatedQty = updateById(state.items, action.id, { qty: action.qty });
    const itemsWithUpdatedTotal = updateItemTotal(itemsWithUpdatedQty, action);
    return updateSummaryTotals({...state, items: itemsWithUpdatedTotal});
}

/** Reducer for setting item price */
function itemSetPrice(state, action) {
    const itemsWithUpdatedPrice = updatePrice(state.items, action);
    const itemsWithUpdatedTotal = updateItemTotal(itemsWithUpdatedPrice, action);
    return updateSummaryTotals({...state, items: itemsWithUpdatedTotal});
}

/** Reducer for updating priceDisplay value */
function itemUpdatePriceDisplay(state, action) {
    const itemsWithUpdatedPriceDisplay = formatPriceDisplay(state.items, action);
    return {...state, items: itemsWithUpdatedPriceDisplay};
}

////////////////////////////////////////////////////////////////////////////////
// main reducer

export const items = createReducer(initState, {
    [ITEM_SET_NAME]: itemSetName,
    [ITEM_ADD]: itemAdd,
    [ITEM_DELETE]: itemDelete,
    [ITEM_SET_QTY]: itemSetQty,
    [ITEM_SET_PRICE]: itemSetPrice,
    [ITEM_UPDATE_PRICE_DISPLAY]: itemUpdatePriceDisplay
});
