import Summary from '../components/Summary';
import { connect } from 'react-redux';

/** State to props mapper */
const mapStateToProps = (state) => ({
    summary: state.summary
});

export const SummaryContainer = connect(
    mapStateToProps
)(Summary);
