import { connect } from 'react-redux';
import {
    itemSetQty,
    itemSetPrice,
    itemUpdatePriceDisplay,
    itemSetName,
    itemAdd,
    itemDelete,
} from '../actions';

import ItemList from '../components/ItemList';

const mapStateToProps = (state, ownProps) => ({ items: state.items });

/** Action dispatcher/props mapper */
const mapDispatchToProps = (dispatch, ownProps) => ({
    onNameChange: (name, item) => {

        // dispatching item update and new item add
        // as separate actions
        const isDirty = item.dirty;
        dispatch(itemSetName(item.id, name));
        if (!isDirty) {
            dispatch(itemAdd());
        }
    },
    onDelete: (item) => {
        dispatch(itemDelete(item.id));
    },
    onQtyChange: (qty, item)  => {
        dispatch(itemSetQty(item.id, qty));
    },
    onPriceChange: (priceDisplay, item)  => {
        dispatch(itemSetPrice(item.id, priceDisplay));
    },
    onPriceExit: (priceDisplay, item) => {
        dispatch(itemUpdatePriceDisplay(item.id, priceDisplay));
    }
});

export const ItemListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemList);

export default ItemListContainer;
