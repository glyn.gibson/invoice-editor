import React from 'react';
import PropTypes from 'prop-types';
import { displayAsCurrency } from './Item';

/**
 * Summary Component
 *
 * Takes object w/ following props
 * @param summary - summary state slice
 * @returns React component
 */
const Summary = ({summary}) => (
    <div className="row upper-spacing">
        <div className="col s12 m4 offset-m8">
            <div className="divider"/>
            <div className="row section tighten-row">
                <div className="col s6">Subtotal</div>
                <div className="col s6 summary-truncate">{displayAsCurrency(summary.subtotal)}</div>
            </div>
            <div className="divider"/>
            <div className="row section tighten-row">
                <div className="col s6">Tax (% {summary.taxRate * 100})</div>
                <div className="col s6 summary-truncate">{displayAsCurrency(summary.tax)}</div>
            </div>
            <div className="divider"/>
            <div className="row section tighten-row">
                <div className="col s6">Total</div>
                <div className="col s6 summary-truncate">{displayAsCurrency(summary.total)}</div>
            </div>
            <div className="divider"/>
        </div>
    </div>
);

Summary.propTypes = {
    summary: PropTypes.shape({
        subtotal: PropTypes.number.isRequired,
        tax: PropTypes.number.isRequired,
        taxRate: PropTypes.number.isRequired,
        total: PropTypes.number.isRequired
    })
};

export default Summary;
