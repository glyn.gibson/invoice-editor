import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

/**
 * Helper for rendering fields as currency strings
 * @param value - value to render
 * @returns currency string w/ fixed decimal and `$` prefix
 */
export function displayAsCurrency(value) {
    const inDollars = isNaN(value) ? '' : (value / 100).toFixed(2);
    return '$ ' + inDollars;
}

/**
 * Item component
 *
 * Takes a configuration object with the following props:
 *
 * @param name - item name
 * @param qty - item quantity
 * @param price - item internal price value
 * @param priceDisplay - value for price display
 * @param total - item total
 * @param dirty - whether or not the item has been edited
 * @param index - position in the items list
 * @param onDelete - item delete event handler
 * @param onNameChange - item name change handler
 * @param onQtyChange - item qty change handler
 * @param onPriceChange - item price change handler
 * @param onPriceExit - handler for when user leaves price input
 * @returns React component
 */
const Item = ({
    name,
    qty,
    price,
    priceDisplay,
    total,
    dirty,
    index,
    onDelete,
    onNameChange,
    onQtyChange,
    onPriceChange,
    onPriceExit
}) => {
    const nameLabel = !dirty ? 'New Item' : '';
    const hiddenFields = !dirty ? 'hidden' : '';
    const background = 'row tighten-row ' + (index % 2 === 0 ? 'grey lighten-5' : '');

    /**
     * Manually add/remove .valid/.invalid styling
     *
     * Workaround added because Materialize doesn't seem to mark the field as
     * invalid if it's empty, even if it's tagged as `required` and/or has an
     * `pattern` attribute.  Unfortunately, turning off Materialize's validation
     * means we have to do rest of validation for Qty ourselves.
     *
     * @param target - target element
     * @param isValid - whether or not the input state is valid
     */
    const setValidity = (target, isValid) => {
        if (isValid) {
            target.classList.remove('invalid');
            target.classList.add('valid');
        } else {
            target.classList.remove('valid');
            target.classList.add('invalid');
        }
    };

    /**
     * Validation handler for qty field
     *
     * Mark as invalid if field is empty/NaN/<= 0/ >= 10^7
     * @param evt - event fired
     */
    const validateQty = evt => {
        const isValid = !_.isEmpty(evt.target.value) &&
                        !isNaN(evt.target.value) &&
                        parseFloat(evt.target.value) > 0 &&
                        parseFloat(evt.target.value) < 10000000;

        setValidity(evt.target, isValid);
    };

    /**
     * Validation handler for name field
     *
     * Mark as invalid if field is empty, except for when field is pristine
     * @param evt - event fired
     */
    const validateName = evt =>  {
        if (!dirty) {
            return;
        }
        const isValid = !_.isEmpty(evt.target.value);
        setValidity(evt.target, isValid);
    };

    return (
        <div className={background}>
            <div className="input-field col s12 m5">
                <label htmlFor="name">{ nameLabel }</label>
                <input type="text" name="name" value={name} onChange={onNameChange} onBlur={validateName}/>
            </div>
            <div className={ hiddenFields }>
                <div className="input-field col s3 m2">
                    <input type="number" name="qty" value={qty} onChange={onQtyChange} onBlur={validateQty} min="0" max="1000000" step="any" required/>
                </div>
                <div className="input-field col s3 m2">
                    <input className="validate" type="text" name="price" value={priceDisplay} onChange={onPriceChange} onBlur={onPriceExit} maxLength="7" required/>
                </div>
                <div className="input-field col s3 m2">
                    <input type="text" name="total" value={displayAsCurrency(total)} readOnly="readonly" />
                </div>
                <div className="input-field col s3 m1">
                    <a className="btn-floating btn-tiny waves-effect waves-light red" name="delete" onClick={onDelete}>
                        <i className="material-icons small">delete</i>
                    </a>
                </div>
            </div>
        </div>
    );
};

Item.propTypes = {
    name: PropTypes.string.isRequired,
    qty: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    priceDisplay: PropTypes.string.isRequired,
    total: PropTypes.number.isRequired,
    dirty: PropTypes.bool.isRequired,
    index: PropTypes.number.isRequired,
    onDelete: PropTypes.func.isRequired,
    onNameChange: PropTypes.func.isRequired,
    onQtyChange: PropTypes.func.isRequired,
    onPriceChange: PropTypes.func.isRequired,
    onPriceExit: PropTypes.func.isRequired
};

export default Item;
