import React from 'react';
import { SummaryContainer } from '../containers/SummaryContainer';
import { ItemListContainer } from '../containers/ItemListContainer';
import './App.css';

/** Returns main App Component */
const App = () => (
    <div className="container z-depth-1 App-container">
        <ItemListContainer />
        <SummaryContainer/>
    </div>
);

export default App;
