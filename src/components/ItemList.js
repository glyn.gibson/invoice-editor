import React from 'react';
import PropTypes from 'prop-types';
import Item from './Item';

/**
 * ItemList component
 *
 * Takes a configuration object with the following props:
 * @param items - items to render
 * @param onNameChange - name change handler
 * @param onDelete - item delete handler
 * @param onQtyChange - quantity change handler
 * @param onPriceChange - price change handler
 * @param onPriceExit - event handler for exiting price field
 * @returns React Component
 */
const ItemList = ({
    items,
    onNameChange,
    onDelete,
    onQtyChange,
    onPriceChange,
    onPriceExit
}) => (
    <form className="col s12">
        <div className="row">
            <div className="col s12">
                <h5>Invoice Editor</h5>
            </div>
        </div>

        {/* faking a tabular layout w/ the grid system seems to work better for
        responsiveness than an actual table */}
        <div className="row form-header tighten-row">
            <div className="col s5">Name</div>
            <div className="col s2">Qty</div>
            <div className="col s2">Price</div>
            <div className="col s1">Total</div>
        </div>
        {items.map((item, index) =>
            <Item
            key={item.id}
            index={index}
            {...item}
            onNameChange={(evt) => onNameChange(evt.target.value, item)}
            onDelete={() => onDelete(item)}
            onQtyChange={(evt) => onQtyChange(evt.target.value, item) }
            onPriceChange={(evt) => onPriceChange(evt.target.value, item) }
            onPriceExit={(evt) => onPriceExit(evt.target.value, item)}
            />
        )}
    </form>
);

Item.propTypes = {
    items: PropTypes.array,
    onDelete: PropTypes.func.isRequired,
    onNameChange: PropTypes.func.isRequired,
    onQtyChange: PropTypes.func.isRequired,
    onPriceChange: PropTypes.func.isRequired,
    onPriceExit: PropTypes.func.isRequired
};

export default ItemList;
