/* @flow */

////////////////////////////////////////////////////////////////////////////////
// action types

export const ITEM_SET_QTY = 'ITEM_SET_QTY';
export const ITEM_SET_PRICE = 'ITEM_SET_PRICE';
export const ITEM_SET_NAME = 'ITEM_SET_NAME';
export const ITEM_DELETE = 'ITEM_DELETE';
export const ITEM_ADD = 'ITEM_ADD';
export const ITEM_UPDATE_PRICE_DISPLAY = 'ITEM_UPDATE_PRICE_DISPLAY';

////////////////////////////////////////////////////////////////////////////////
// action creators

// this seems easier than tracking index position
let itemId = 0;

export const itemSetQty = (id, qty) => ({
    type: ITEM_SET_QTY, id, qty
});

export const itemSetPrice = (id, price) => ({
    type: ITEM_SET_PRICE, id, price
});

export const itemSetName = (id, name) => ({
    type: ITEM_SET_NAME, id, name
});

export const itemDelete = (id) => ({
    type: ITEM_DELETE, id
});

export const itemAdd = () => ({
    type: ITEM_ADD,
    id: ++itemId
});

// TODO(ggibson): rename function to something more model-appropriate
export const itemUpdatePriceDisplay= (id) => ({
    type: ITEM_UPDATE_PRICE_DISPLAY, id
});
