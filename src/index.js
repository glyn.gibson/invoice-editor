import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import { createStore } from 'redux';
import invoiceApp from './reducers';

import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';

let store = createStore(invoiceApp);

render (
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();
